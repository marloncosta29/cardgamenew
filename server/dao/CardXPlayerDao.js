var CardXPlayer = require('.././models/CardXPlayer');
var Card        = require('.././models/Card');


//pega as cartas de um usuario
exports.cardsXplayer = function(player, callback)
{	
	CardXPlayer.find({ id_player : player.id }, function(err, data){
		if(err)
			callback(err);
		callback(data);
	});
}


exports.pegaBaralho = function(_id, callback)
{	
	CardXPlayer.find({ id_player : _id }, function(err, data){
		if(err)
			callback(err);
		callback(data);
	});
}

//cria o primeiro baralho
exports.primeiroBaralho = function(id, callback){
	Card.find({}, function(err, data){
		if(err)
			callback(err);
		for(card in data)
		{
			var dados = new CardXPlayer();
			dados.id_player = id;
			dados.card      = data[card];
			dados.inDeck    = true;
			console.log(dados);
			dados.save(function(err, data){
				if(err)
					console.log(err);
				console.log(data);
			});
		}
		
	}).limit(30)

}
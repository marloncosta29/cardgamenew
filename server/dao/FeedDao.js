var Feed = require('.././models/Feed');

exports.salvar = function(data, callback)
{
    var feed = new Feed();
    feed.id_user = data.id;
    feed.icone   = data.icone;      
    feed.titulo  = data.titulo;
    feed.content = data.conteudo;
    feed.day     = data.dia;
    feed.mouth   = data.mes;
    feed.year    = data.ano;

    feed.save(function(err, data){
        if(err)
            callback(err , null);
        callback(null, data);
    });
}


exports.getFeeds = function(callback){
    Feed.find({}, function(err, data){
        if(err)
        {
            console.log(err);
            callback(err);
        }
        console.log(data);
        callback(data);
    })
}

exports.delete = function(data)
{
	Feed.findOneAndRemove({ _id: data.id, id_user : data.id_user }, function(err, data){
		console.log(data);
	});
}
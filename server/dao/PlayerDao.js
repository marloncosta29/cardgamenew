var Player   = require('.././models/Player');
var CardXPlayerDao = require('.././dao/CardXPlayerDao');
var bcrypt = require('bcrypt');

//Faz o login no sistema, necessario reformular
exports.login = function(data, callback)
{
    Player.findOne({ email : data.email }, function(err, user){
        if(err)
        {
            callback(err);
        }
        callback(user);
    });
}

//salva o usuario no sistema
exports.save = function(data, callback)
{
    console.log('entrei no save');
    var player = new Player();
    player.name       = data.name;
    player.nickname   = data.nickname;
    player.email      = data.email;
    player.password   = data.password;
    player.experience = 0;
    player.nivel      = 0;
	player.win        = 0;
	player.lose       = 0;
    player.isPlaying  = false;
    player.perfilImage= 'images\\perfil\\HuehuePerfil.png';
	player.history    = '';

    player.save(function(err, data){
        if(err)
            callback(err ,  null);
        console.log('DADOS DO SAVE');
        console.log(data);
        CardXPlayerDao.primeiroBaralho(data._id);
        callback(null, data);
    });
}

//find One
exports.findOne = function(user, callback)
{
    Player.findOne({ email : user.username }, function(err, user){
        if(err)
        {
            callback(err, null);
        }   
        callback(null, user);
    });
}

//validPassword
exports.validPassword = function(password, user)
{
    if(password == user.password)
        return true;
    return false;
}

//find  by id
exports.getJogadorById = function(id, callback)
{
    Player.findOne({ _id : id }, function(err, user){
        if(err)
        {
            callback(err, null);
        }
        callback(null, user);
    });
}

//hash
exports.geraHash = function(email){
    const saltRounds = 5;         
        bcrypt.genSalt(saltRounds, function(err, salt) {
            bcrypt.hash(email, salt, function(err, hash) {
                if(err)
                    return err;
                return hash;
            });
        });

}

exports.perdeuJogo = function(dados, callback){
    /*
        Aqui sera a  função que sera chamada quando o jogador perder
        Regras
            jogador ira receber 500 de XP
            jogaor nao ganha recopensas
    */
    Player.findOne({ _id : dados.id_user }, function(err, user){
        if(err)
        {
            callback(err, null);
        } 
        var experience = dados.pontos;
        calculaExperiencia(user, experience, function(data){
             callback(data);
        });
		somaVitotiaDerrota(user, false, true, function(data){
			console.log(data)
		})
    });    
}

exports.ganhouJogo = function(dados, callback){
    /*
        Aqui sera a  função que sera chamada quando o jogador perder
        Regras
            jogador ira receber 1000 de XP
            jogador ganha recopensas
    */
    Player.findOne({ _id : dados.id_user }, function(err, user){
        if(err)
        {
            callback(err, null);
        } 

        var experience = dados.pontos;
        calculaExperiencia(user, experience, function(data){
            callback(data);
        }) 
		somaVitotiaDerrota(user, true, false, function(data){
			console.log(data)
		})
    });
}

exports.alteraDados = function (dados, callback){
	console.log('Mudando dados');
	console.log(dados);
	Player.findOne({ _id : dados.id_user }, function(err, user){
        if(err)
        {
            callback(err, null);
        }

		Player.update({ _id: user._id }, { $set: { experience: newExperience, nivel : newNivel }}, function(data){
			callback(data);
		});
	});
}

exports.ranking = function(callback)
{
	/*
		Metodo para trazer os jogadores que estão no topo
		É um top 10
	*/
	
	//Player.find({}).sort({ "win": 1 })
	Player.find({}).limit(10).sort({ 'win' : -1}).exec(function(err, dados){ 
		console.log(err);
		console.log(dados);
		callback(dados);
	});
}

exports.trocaImgPerfil = function(data){
	Player.update({ _id: data.id }, { $set: { perfilImage : data.url }}, function(data){
            console.log(data);
        });
}


function calculaExperiencia(user, experience, callback){
    /*
    console.log('-------------------------------------');
    console.log('GERANDO EXPERIENCIA');
    console.log(user);
    console.log(experience);
    */

    var newNivel = user.nivel;
    var newExperience = user.experience + experience;
    //console.log('TOTAL EXPERIENCIA' + newExperience);
    while(newExperience >=1000)
    {
        newNivel = newNivel + 1;
        newExperience = newExperience - 1000;
    }
    //console.log('Novos Dados')
    //console.log(newNivel);
    //console.log(newExperience);

    Player.update({ _id: user._id }, { $set: { experience: newExperience, nivel : newNivel }}, function(data){
        callback(data);
    });
}


function somaVitotiaDerrota(user, vitoria, derrota, callback)
{
	if(vitoria)
	{
		var vitorias = user.win + 1;
		
		Player.update({ _id: user._id }, { $set: { win : vitorias }}, function(data){
			callback(data);
		});
	}
	
	if(derrota)
	{
		var derrotas = user.lose + 1;
		
		Player.update({ _id: user._id }, { $set: { lose : derrotas }}, function(data){
			callback(data);
		});
	}
}
















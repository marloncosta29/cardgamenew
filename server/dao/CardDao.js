var Card = require('.././models/Card');

exports.salvar = function(data, callcack)
{
	//criando o objeto carta
	var card = new Card();

	//atribuindo os atributos ao objeto
	card.name    = data.nome;
    card.type    = data.tipo;
    card.attack  = data.ataque;
    card.defense = data.defesa;
    card.cost    = data.custo;
    card.image   = data.imagemCarta;
    card.abiity  = data.habilidade;

    //persistindo os dados no banco
    card.save(function(err, data){
    	if(err)
			callcack(err);
		callcack(data);
    })
}

exports.getCartas = function(callback){
    console.log('entrei no getCartas');
    Card.find({}, function(err, data){
        if(err)
        {
            console.log(err);
            callback(err);
        }
        console.log(data);
        callback(data);
    })
}
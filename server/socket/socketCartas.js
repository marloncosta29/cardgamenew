var fs        = require('fs');
var CardDao   = require('../.././server/dao/CardDao');
var CardXPlayerDao = require('../.././server/dao/CardXPlayerDao');

module.exports = function(io)
{
	//Inicio do IO
	io.sockets.on("connection", function (socket) {

		//cadastro de cartas
		socket.on('cadastrarCarta', function(data, callback){

	        var cardName = 'client/images/cartas/' + data.nome + '.jpg';
	        var decodedImage = new Buffer(data.imagemCarta, 'base64');    
	        fs.writeFile(cardName, decodedImage, function(err) {});

	        data.imagemCarta = 'images/cartas/' +  data.nome + '.jpg';

	        CardDao.salvar(data, function(err, carta){
	        	if(err)
	        		callback(err);
	        	callback(carta);
	        });
        });

		//consulta de cartas
		socket.on('consultaCartas', function(data, callback){
			 CardDao.getCartas(function(data){
			 	 callback(data);
			 });	 
		});

		socket.on('baralhoJogador', function(data, callback){
			 CardXPlayerDao.pegaBaralho(data, function(data){
			 	 callback(data);
			 });	 
		});

		//cartas do jogador
		socket.on('cartasDoJogador', function(data, callback){
			CardXPlayerDao.cardsXplayer(data, function(data){
				callback(data);
			})
		});




    //Fim do IO
	});
}
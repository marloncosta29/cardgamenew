var Player         = require("../.././server/models/Player");
var Game           = require("../.././server/models/Game");
var PlayerDao      = require('../.././server/dao/PlayerDao');
var CardXPlayerDao = require('../.././server/dao/CardXPlayerDao');

var SOCKET_LIST = {};
var PLAYER_LIST = {};
var ROMS_LIST = {};

var bcrypt = require('bcrypt');


module.exports = function(io)
{
    //Iniciando o Socket
    io.sockets.on("connection", function (socket) {

        socket.on('dataToList', function(data){
                //console.log(PLAYER_LIST[socket.id]);
                data.socket_id = socket.id;
                PLAYER_LIST[socket.id] = data;
                console.log('APOS ADD USER NA LISTA');
                console.log(PLAYER_LIST[socket.id]);
        });

        //Colocando o Socket em uma lista, para uso posterior
            //console.log(SOCKET_LIST[socket.id]);
            SOCKET_LIST[socket.id] = socket;

        //Emitindo os dados do Jogador
        socket.emit('player', PLAYER_LIST[socket.id]);

        //Escutadno o evento de desafio
        socket.on('toChallenger', function (data) {
            console.log('ENTRANDO NO CHALLENDER... DEVE TER 2')
            //Desafiando o Jogador adversario, usando o ID do Socket
            SOCKET_LIST[data.challenged.socket_id].emit('challenging', data);
        });

        //Desafiado aceita o desafio
        socket.on('accept', function (data) {
            //console.log('ENTREI NO ACEPPT');
            //console.log(data);
            data.challenger.user.opponent = data.challenged.id;
            data.challenged.user.opponent = data.challenger.id;
            data.challenger.user.isPlaying = true;
            data.challenged.user.isPlaying = true;
            PLAYER_LIST[data.challenger.socket_id].isPlaying = true;
            PLAYER_LIST[data.challenged.socket_id].isPlaying = true;
            
            ROMS_LIST[data.id] = data;

            CardXPlayerDao.pegaBaralho(data.challenger.user._id, function(deck){
                shuffle(deck);
                data.challenger.deck = deck;
                SOCKET_LIST[data.challenger.socket_id].emit('gameStart', data);
            })

            CardXPlayerDao.pegaBaralho(data.challenged.user._id, function(deck){
                shuffle(deck);
                data.challenged.deck = deck;
                SOCKET_LIST[data.challenged.socket_id].emit('gameStart', data);
            })
        });
        
        //Desconectadno o Socket    
        socket.on('disconnect', function (data)
        {
            //PLAYER_LIST = Game.removePlayer(PLAYER_LIST, socket);
            delete PLAYER_LIST[socket.id];
            delete SOCKET_LIST[socket.id];
            if(ROMS_LIST[data.id_room] != null)
                delete ROMS_LIST[data.id_room];
			//console.log(PLAYER_LIST);
        });

        //Dados para o oponete
        socket.on('dados_para_oponente', function(data){
            //console.log('DADOS PARA OPONETE');
            //console.log(data);
            try{
                if(ROMS_LIST[data.id_room].challenger.socket_id == data.paraOponete.socket_id)
                {
                    //console.log('EMITINDO EVENTO CHALANGED');
                    SOCKET_LIST[ROMS_LIST[data.id_room].challenged.socket_id].emit('dados_do_oponente', data);
                }

                //console.log(ROMS_LIST[data.id_room].challenged.socket_id +'  -  '+ data.paraOponete.socket_id);
                if(ROMS_LIST[data.id_room].challenged.socket_id == data.paraOponete.socket_id)
                {
                    //console.log('EMITINDO EVENTO CHALANGER');
                    SOCKET_LIST[ROMS_LIST[data.id_room].challenger.socket_id].emit('dados_do_oponente', data);
                }


            }catch(err)
            {
                
            }
            
        });
        socket.on('cardsParaOponete', function(data){

            if(ROMS_LIST[data.id_room].challenger.socket_id == data.paraOponete.socket_id)
            {
                //console.log('EMITINDO EVENTO CHALANGED');
                SOCKET_LIST[ROMS_LIST[data.id_room].challenged.socket_id].emit('cartas_do_oponente', data.cards);
            }

            if(ROMS_LIST[data.id_room].challenged.socket_id == data.paraOponete.socket_id)
            {
                SOCKET_LIST[ROMS_LIST[data.id_room].challenger.socket_id].emit('cartas_do_oponente', data.cards);
            }
        });

        //Troca de turno
        socket.on('troca_turno', function(data){

            var newTurnoAtivo;
            if(data.turnoAtivo == data.dados.challenger.socket_id)
            {
                newTurnoAtivo = data.dados.challenged.socket_id;
            }

            if(data.turnoAtivo == data.dados.challenged.socket_id)
            {   
                newTurnoAtivo = data.dados.challenger.socket_id;
            }

            SOCKET_LIST[data.dados.challenger.socket_id].emit('novo_turno', newTurnoAtivo);
            SOCKET_LIST[data.dados.challenged.socket_id].emit('novo_turno', newTurnoAtivo);
        });

        socket.on('atacando', function(data){
            //console.log('ATANCANDO COM A CARTA ' +  data.card.name);
            //console.log(ROMS_LIST[data.id_room]);
            if(ROMS_LIST[data.id_room].challenger.socket_id == data.socket_id)
            {
                SOCKET_LIST[ROMS_LIST[data.id_room].challenged.socket_id].emit('recebendo_ataque', data);
            }

            if(ROMS_LIST[data.id_room].challenged.socket_id == data.socket_id)
            {
                SOCKET_LIST[ROMS_LIST[data.id_room].challenger.socket_id].emit('recebendo_ataque', data);
            }
        });

        socket.on('destroi_carta_campo_oponente', function(data){
            console.log('DESTRUINDO A CARTA ')
            console.log(data);
            console.log(socket.id +" VS  "+data.socket_challenger)
            console.log(socket.id +" VS  "+data.socket_challenged)
            if(socket.id == data.socket_challenger)
            {
                //console.log(SOCKET_LIST[data.socket_challenged]);
                SOCKET_LIST[data.socket_challenged].emit('Destruindo_carta' , data.posN);
            }
            if(socket.id == data.socket_challenged)
            {
                //console.log(SOCKET_LIST[data.socket_challenger])
                SOCKET_LIST[data.socket_challenger].emit('Destruindo_carta' , data.posN);   
            }
        });

        /*
            Recebe o id do jogador e retorno o mesmo com os dados atualizados
        */
        socket.on('atalizaDados', function(data, callback){
            PlayerDao.findOne(data, function(err, user){
                if(err)
                    callback(err, null);
                callback(null, user);
            });
        });


        socket.on('fimDeJogo', function(data){
            //console.log('Dados da ROOM')
            //console.log(ROMS_LIST[data.id_room]);


            if(SOCKET_LIST[ROMS_LIST[data.id_room].challenger.socket_id].id == data.socket_id)
            {

                ptGanhador = Math.floor((Math.random() * 500) + 500);
                ptPerdedor = Math.floor((Math.random() * 500) + 0);


                SOCKET_LIST[ROMS_LIST[data.id_room].challenger.socket_id].emit('perdeu',{ pontos : ptPerdedor});
                SOCKET_LIST[ROMS_LIST[data.id_room].challenged.socket_id].emit('ganhou',{ pontos : ptGanhador});
                
                challenger = {
                    id_user : ROMS_LIST[data.id_room].challenger.user._id,
                    pontos  : ptPerdedor
                }

                challenged = {
                    id_user : ROMS_LIST[data.id_room].challenged.user._id,
                    pontos  : ptGanhador
                }


                PlayerDao.perdeuJogo(challenger, function(err, data){
                    if(err);
                        console.log(err)
                    console.log(data); 
                });
                PlayerDao.ganhouJogo(challenged, function(err, data){
                    if(err);
                        console.log(err)
                    console.log(data); 
                });
            }

            if(SOCKET_LIST[ROMS_LIST[data.id_room].challenged.socket_id].id == data.socket_id)
            {

                ptGanhador = Math.floor((Math.random() * 500) + 500);
                ptPerdedor = Math.floor((Math.random() * 500) + 0);

                SOCKET_LIST[ROMS_LIST[data.id_room].challenged.socket_id].emit('perdeu',{ pontos : ptPerdedor});
                SOCKET_LIST[ROMS_LIST[data.id_room].challenger.socket_id].emit('ganhou',{ pontos : ptGanhador});


                challenger = {
                    id_user : ROMS_LIST[data.id_room].challenged.user._id,
                    pontos  : ptPerdedor
                }

                challenged = {
                    id_user : ROMS_LIST[data.id_room].challenger.user._id,
                    pontos  : ptGanhador
                }


                PlayerDao.perdeuJogo(challenged,function(err, data){
                    if(err);
                        console.log(err)
                    console.log(data); 
                });
                PlayerDao.ganhouJogo(challenger,function(err, data){
                    if(err);
                        console.log(err)
                    console.log(data); 
                });
            }


            delete ROMS_LIST[data.id_room];
        });
		
		socket.on('getRanking', function(data, callback){
			console.log('CHEGUEI NO EVENTO DE RANKING')
			PlayerDao.ranking(function(dados){
				callback(dados);
			})
		});
		
		socket.on('trocaImgPerfil', function(data){
            console.log('ENTREI NA TROCA DE IMAGEM')
			PlayerDao.trocaImgPerfil(data);
		});
		
    });

}

//intervalo de emissão dos jogadores online
setInterval(function ()
{
    for (var i in SOCKET_LIST)
    {   
        var socket = SOCKET_LIST[i];
        socket.emit('players', PLAYER_LIST);
        socket.emit('rooms', ROMS_LIST);
    }
    cont = 0;
}, 10000/25)


function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}
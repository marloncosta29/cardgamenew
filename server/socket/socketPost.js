var fs        = require('fs');
var PlayerDao      = require('../.././server/dao/FeedDao');
module.exports = function(io)
{
	//Inicio do IO
	io.sockets.on("connection", function (socket) {
    	
    	//Salvando o Post
    	socket.on('post', function(data, callback){
    		console.log(data);
    		PlayerDao.salvar(data, function(err, retorno){
    			if(err)
    				callback(err);
    			callback(retorno);
    			getFeeds();
    		});
    	})

    	function getFeeds()
    	{
    		PlayerDao.getFeeds(function(data){
    			socket.emit('getFeeds', data);
    			socket.broadcast.emit('getFeeds', data);
    		});
    		
    	}

    	socket.on('carregaFeed', function(data){
    		PlayerDao.getFeeds(function(data){
    			socket.emit('getFeeds', data);
    		});
    	})

    //Fim do IO	
	});
}

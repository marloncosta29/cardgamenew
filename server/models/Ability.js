var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var Ability   = new Schema({
	id_player : String,
	id_card   : String
});

module.exports = mongoose.model('Ability', Ability);
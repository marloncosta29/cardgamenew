var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var Feed   = new Schema({
	id_user     : String,
	icone       : String,
	titulo      : String,
    content     : String,
    day         : { type: Number },
    mouth       : { type: Number },
    year        : { type: Number },
});

module.exports = mongoose.model('Feed', Feed);
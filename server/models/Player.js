﻿var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var PlayerSchema   = new Schema({
	idSocket   : String,
    name       : String,
    nickname   : String,
    email      : String,
    password   : String,
    experience : { type: Number },
    nivel      : { type: Number },
	win        : { type: Number },
	lose       : { type: Number },
    isPlaying  : Boolean,
    perfilImage: String,
	history    : String
});

module.exports = mongoose.model('Player', PlayerSchema);
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var CardSchema   = new Schema({
    name      : String,
    type      : String,
    attack    : String,
    defense   : String,
    cost      : { type: Number },
    image     : String,
    abiity    : { type: Number } 
});

module.exports = mongoose.model('Card', CardSchema);
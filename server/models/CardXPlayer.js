var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var CardXPlayer   = new Schema({
	id_player : String,
	card      : Object,
	inDeck    : Boolean,
});

module.exports = mongoose.model('CardXPlayer', CardXPlayer);
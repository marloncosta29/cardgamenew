﻿var express = require("express");
var router  = express.Router();
var PlayerDao = require('.././dao/PlayerDao');

//GET USER DATA
router.get('/userData', function(req, res){
    console.log('USER DATA');
    console.log(req.user.email);
    var hash = PlayerDao.geraHash(req.user.email);

    var data = {
        userData : req.user,
        hash     : hash
    }
    console.log(data);
    res.send(data);
});
//SAVE USER
router.get('/cadastro', function(req, res){
    res.render('partials/cadastro');
});

router.post('/cadastro', function(req, res){
    //console.log('DADOS POST');
    //console.log(req.body);
    PlayerDao.save(req.body, function(err, data){
        if(err)
        {
            var msg = 'OPS HOUVE UM PROBLEMA NO SEU CADASTRO ' + err;
            res.render('partials/cadastro', { message :  msg });
        }else
        {
            res.render('home', { message : 'CADASTRO FEITO COM SUCESSO'});
        }
            
    });
});


//GET HOME
router.get('/', function (req, res) {
    //res.sendFile('c:/Projetos/cardgamenew/client/views/index.html');
    if(req.user)
    {
       res.render('perfil', { user : req.user});
    }else
    {
      res.render('home');
    }

});

//GET PERFIL
router.get('/perfil', function(req, res){
    if(req.user)
    {
       //console.log('Dados do Usuario do REQ')
       //console.log(req.user)
       res.render('perfil');
    }else
    {
      //console.log(req.user)
      res.render('home');
    }


});

//PASSPORT.JS
var passport      = require('passport')
var LocalStrategy = require('passport-local').Strategy;
passport.use(new LocalStrategy(
  function(username, password, done) {
    //achando o usuario
    
    PlayerDao.findOne({ username: username }, function (err, user) 
    {
      if (err) 
      { 
          return done(err); 
      }
    //se nao haser usuario retorna erro
      
      if(!user)
      {
        return done(null, false, { message: 'Incorrect username.' });
      }
    //se nao verifica se o password é valido
    if(!PlayerDao.validPassword(password, user))
    {
      return done(null, false, { message: 'Incorrect password.' });
    }  
    //se for retorna o user
      return done(null, user);
    });
  }
));
//serializando o user
passport.serializeUser(function(user, done) {
    //console.log('Serializei o : ' + user);
    done(null, user._id);
});
//descerializando o user
passport.deserializeUser(function(id, done) {
    //console.log('tentando deserializar o ' + id);
    PlayerDao.getJogadorById(id, function(err, user) {
        //console.log("Descerializei o : " + user);
        done(err, user);
    });
});

//rota para logar usando o passport
router.post('/login', passport.authenticate('local', { successRedirect: '/perfil', failureRedirect: '/' }));

router.get('/logout', function(req, res) {
    
    console.log('INICIO DO LOGOUT');
    //req.flash('success_msg', 'You are logged out');
    
    req.session.destroy();
    
    res.redirect('/');
    console.log('FIM DO LOGOUT');
});

module.exports = router;


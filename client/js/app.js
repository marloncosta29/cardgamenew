﻿
angular.module('app', ['ui.router', 'ngFileUpload'])

.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

        //$urlRouterProvider.otherwise('/erro');

        $stateProvider
            //.state('home'          , { url: '/'             , templateUrl: 'views/partials/perfil.html'          , controller: 'perfilCtrl'      })
            .state('perfil'        , { url: '/perfil'       , templateUrl: 'views/partials/perfil.html'        , controller: 'perfilCtrl'     })
            //.state('login'         , { url: '/login'        , templateUrl: 'views/partials/login.html'         , controller: 'LoginCtrl'      })
            //.state('cadastro'      , { url: '/cadastro'     , templateUrl: 'views/partials/cadastro.html'      , controller: 'SavePlayerCtrl' })
            //.state('contato'       , { url: '/cocmdntato'      , templateUrl: 'views/partials/contact.html'})
            .state('game'          , { url: '/game'         , templateUrl: 'views/partials/game.html'          , controller: 'gameController' , params:{ dados : null} })
            //.state('admin'         , { url: '/admin'        , templateUrl: 'views/partials/admin.html'         , controller: 'adminCtrl'})
            //.state('cartaCadastro' , { url: '/cartaCadastro', templateUrl: 'views/partials/cartaCadastro.html' , controller: 'cadastraCartaCtrl' })
            //.state('cartaConsulta' , { url: '/cartaConsulta', templateUrl: 'views/partials/consultaCartas.html', controller: 'consultaCartaCtrl' });
})
.run(['$rootScope', function ($rootScope) {    
   $rootScope.player  = null;
   $rootScope.cards   = null;
   $rootScope.baralho = null;
}]);

angular.module('app').controller('socket', function ($scope, $location) {
   var socket = io.connect('http://localhost:1337', {});
   $scope._socket = socket;   
   $scope.users = [];
   $scope.rooms = [];
   $scope.data = null;
});
angular.module('app').controller('gameController', function ($location, $state, $scope, $stateParams) {
    console.log('CARREGANDO O CONTROLLER GAME');
    console.log($stateParams);

    $scope.dados = $stateParams.dados
    //console.log('DADOS DO SCOPE');
    console.log($scope.dados);
    $scope.turnoAtivo = null;
    var cards = [];
    var cartasGeladeira = [];
    var cardsAux = [];
    var button;
    var pontosDeVida = 5;
    var pontosDeIbope= 20;
    var pontosDeIbopeAntes = 3;
    var poppup;
    var btn_turno;
    var deck;
    var contador = 0;
    var comprou  = false;
    //dados Oponente
    var cardsParaOponete = {};
    var oponenteVida;
    var oponenteIbope;
    var oponenteVidaNum;
    var oponenteIbopeNum;
    var oponenteCartasDeck;
    var oponenteGeladeira;
    var style = { font: "bold 20px Comic Sans MS", fill: "#006400", boundsAlignH: "center", boundsAlignV: "middle" };
    var stylePequeno = { font: "bold 15px Comic Sans MS", fill: "#006400", boundsAlignH: "center", boundsAlignV: "middle" };    
    var styleGrande  = { font: "bold 30px Comic Sans MS", fill: "#006400", boundsAlignH: "center", boundsAlignV: "middle" };    
    
    //cartas na mao
    var cardsHand = [
        { PosX: 50, PosY:  430, card: null },
        { PosX: 170, PosY: 430, card: null },
        { PosX: 290, PosY: 430, card: null },
        { PosX: 415, PosY: 430, card: null },
        { PosX: 535, PosY: 430, card: null }];
    //cartas do campo do jogador
    var cardsOnField = [
        { PosX: 50 , PosY: 250, ocupada: false, card : null},
        { PosX: 170, PosY: 250, ocupada: false, card : null},
        { PosX: 290, PosY: 250, ocupada: false, card : null},
        { PosX: 415, PosY: 250, ocupada: false, card : null},
        { PosX: 535, PosY: 250, ocupada: false, card : null}];
    //variavel do jogo
    var cardsOnFieldOponent = [
        { PosX: 50 , PosY: 50, ocupada: false, card : null},
        { PosX: 170, PosY: 50, ocupada: false, card : null},
        { PosX: 290, PosY: 50, ocupada: false, card : null},
        { PosX: 415, PosY: 50, ocupada: false, card : null},
        { PosX: 535, PosY: 50, ocupada: false, card : null}];
    //variavel do jogo
    var game = new Phaser.Game( 800, 600, Phaser.AUTO, 'game', { preload: preload, create: create, update: update });
    
    //--METODOS USADOS PELO PHASER--

    function testeLoad()
    {
        console.log('carregando')
    }
    function progress(progress)
    {
        console.log(progress);
    }
    function conplete()
    {
        console.log('load complete');
    }

    //PRELOAD PHASER
    function preload() {
        game.load.onLoadStart.add(testeLoad, this);
        game.load.onFileComplete.add(progress, this);
        game.load.onLoadComplete.add(conplete, this);

        game.stage.backgroundColor = 0xbada55;
        game.load.bitmapFont('carrier_command', 'bitmapFonts/carrier_command.png', 'bitmapFonts/carrier_command.xml');

        game.load.image('bg'                   , 'images/assets/bg.jpg')
        game.load.image('cartasPosicao'        , 'images/assets/cartasPosicao.png');
        game.load.image('deck'                 , 'images/assets/deck.png');
        game.load.image('card'                 , 'images/assets/card.png');
        game.load.image('cardPopup'            , 'images/assets/cardPopup.png');
        game.load.spritesheet('btn_atacar'     , 'images/assets/btn_atacar.png', 100, 40);
        game.load.spritesheet('btn_summonar'   , 'images/assets/btn_summonar.png', 100, 40);
        game.load.spritesheet('btn_descartar'  , 'images/assets/btn_descartar.png', 100, 40);
        game.load.spritesheet('btn_dadosCarta' , 'images/assets/btn_dadosCarta.png', 100, 40);
        game.load.spritesheet('btn_turno'      , 'images/assets/btn_troca_turno.png', 200, 40);
        game.load.image('botaoFecharDadosCarta', 'images/assets/fecharData.png');
        game.load.image('telaAtaque'           , 'images/assets/card_ataque_fundo.png');
        game.load.spritesheet('btn_recebendo_ataque' , 'images/assets/btn_receber_dano.png', 200, 40);
        game.load.image('msg_small'            , 'images/assets/sprite_msg_small.png');
        game.load.image('msg_final'            , 'images/assets/msg_final.png');
        game.load.spritesheet('btn_volta_para_perfil', 'images/assets/btn_volta_para_perfil.png', 300, 40);
        game.load.image('perdeu_why'            , 'images/assets/perdeu_why.png');
        game.load.image('ganhei_uhuu'            , 'images/assets/ganhei_uhuu.png');
        game.load.image('ganhei_uhuu'            , 'images/assets/ganhei_uhuu.png');
        game.load.spritesheet('btn_pesquisa_jogo', 'images/assets/btn_pesquisa_jogo.png', 300, 40);
        

        $scope.turnoAtivo = $scope.dados.challenger.socket_id;
        
        if($scope._socket.id == $scope.dados.challenger.socket_id)
        {
           cards = $scope.dados.challenger.deck;
           game.load.image('perfil_image'         ,  $scope.dados.challenger.user.perfilImage);
        }
        if($scope._socket.id == $scope.dados.challenged.socket_id)
        {
            cards = $scope.dados.challenged.deck;
            game.load.image('perfil_image'         ,  $scope.dados.challenged.user.perfilImage);
        }

        for(i  in cards)
        {
            game.load.image(cards[i].card.name, cards[i].card.image);
        }

        //CONTROLE DE TURNOS
        //gerado o turno aleatoriamente
        //quem começa é o desafiante entao o id do chalenged é o turno ativo
        $scope.turnoAtivo = $scope.dados.challenged.socket_id;
        dadosOponente();
    }
    
    //CREATE PHASER
    function updateCounter() {

         contador++;
    }

    function create() {
        game.stage.backgroundColor = '#FFF';
        game.time.events.loop(Phaser.Timer.SECOND, updateCounter, this);
        game.time.events.loop(Phaser.Timer.SECOND, dadosOponente, this);
        //game.time.events.loop(Phaser.Timer.SECOND, geraCartasParaOponente, this);
        
        var bg            = game.add.image(game.world.centerX, game.world.centerY, 'bg').anchor.set(0.5);
        var cartasPosicao = game.add.image(game.world.centerX, game.world.centerY, 'cartasPosicao').anchor.set(0.5);
        deck = game.add.sprite(665, 420, 'deck');
        deck.width = 110;
        deck.height = 160;
        deck.inputEnabled = true;
        deck.events.onInputDown.add(clickOnDeck, this);
        cardOnHand();

        for(var i = 0 ; i < 5 ; i++)
        {
            //retira o ultimo elemento
            var cardData = cards.pop();
            
            // gera a carta com a imagem e os dados
            var card = createCard(cardData, cardsHand[i].PosX, cardsHand[i].PosY);

            card.sprite.posN = i; 
            cardsHand[i].card = card;
            cardsAux.push(card);
        }

        //text = game.add.text(0, 30, "phaser 2.4 text bounds", style);
        text2 = game.add.text(10, 3, "phaser 2.4 text bounds", style);
        jogadorAtivo = game.add.text(305, 212, "phaser 2.4 text bounds", style);
        //MEUS DADOS
        


        line = new Phaser.Line(645, 220, 770, 220);
        var perfilImage         = game.add.image(697, 235, 'perfil_image');
        perfilImage.width       = 40;
        perfilImage.height      = 40;
        pontosDeVidaText    = game.add.text(680, 275, 'Pt. DE VIDA' , style);
        pontosDeVidaNum     = game.add.text(697, 300, pontosDeVida  , styleGrande);
        pontosDeIbopeText   = game.add.text(677, 340, 'Pt. DE IBOPE' , style);
        pontosDeIbopeNum    = game.add.text(697, 365, pontosDeIbope , styleGrande);
        baralhoText         = game.add.text(685, 400, 'Baralho', style);
        cartasNoDeck        = game.add.text(705, 490, cards.length , style);
        
        cartasGeladeiraText = game.add.text(670, 575, cartasGeladeira.length , stylePequeno);
        
        
        btn_turno =  game.add.button(50,205, 'btn_turno', function() {trocaTurno(this)}, this, 1, 0, 2);
    }

    
    //UPDATE PHASER
    function update() {
        
        if(contador == 30 && $scope.turnoAtivo == $scope._socket.id)
        {
            //trocaTurno();
        }

        if($scope.turnoAtivo == $scope._socket.id)
        {
            btn_turno.inputEnabled = true;
            deck.inputEnabled      = true;
        }else{
            btn_turno.inputEnabled = false;
            deck.inputEnabled      = false;

        }

        if($scope.dados != null)
        {
            if($scope.turnoAtivo == $scope.dados.challenger.socket_id)
            {
           jogadorAtivo.text = "TURNO ATIVO : " + $scope.dados.challenger.user.nickname;
            }
            if($scope.turnoAtivo == $scope.dados.challenged.socket_id)
            {
               jogadorAtivo.text = "TURNO ATIVO : " + $scope.dados.challenged.user.nickname;
            }
        }

        


        //text.text               = "X: " + game.input.x + " - Y: "+ game.input.y +" Meu ID é : " + $scope._socket.id;
        text2.text              = "TEMPO DO TURNO : " +  contador;
        pontosDeVidaText.text   = "Pt.VIda";
        cartasNoDeck.text       =  cards.length;
        pontosDeVidaNum.text    = pontosDeVida;
        pontosDeIbopeNum.text   = pontosDeIbope;
        pontosDeIbopeText.text  = "Pt.Ibope";
        cartasGeladeiraText.text= "Geladeira :" + cartasGeladeira.length;
    }

    //METODOS QUE MANADA E RECEBE OS DADOS DO OPONENTE
    
    function dadosOponente()
    {
        //console.log('dados_para_oponente');
        if($scope.dados != null)
        {
            $scope._socket.emit('dados_para_oponente', {
                id_room     : $scope.dados.challenger.socket_id +''+  $scope.dados.challenged.socket_id,
                paraOponete : {
                                socket_id       : $scope._socket.id,
                                pontosDeVida    : pontosDeVida,
                                pontosDeIbope   : pontosDeIbope,
                                cardsNoDeck     : cards.length,
                                cartasGeladeira : cartasGeladeira.length
                             }
            });
        }
    }
    $scope._socket.on('dados_do_oponente', function(data){
            if(oponenteVida)
            {oponenteVida.destroy();}
            if(oponenteVidaNum)
            {oponenteVidaNum.destroy();}
            if(oponenteIbope)
            {oponenteIbope.destroy();}
            if(oponenteIbopeNum)
            {oponenteIbopeNum.destroy();}

            oponenteVida       =  game.add.text(680, 55 , 'Pt.Vida' , style);
            oponenteVidaNum    =  game.add.text(697, 80 ,  data.paraOponete.pontosDeVida, styleGrande);
            
            oponenteIbope      =  game.add.text(677, 120, 'Pt.Ibope' , style);
            oponenteIbopeNum   =  game.add.text(697, 145, data.paraOponete.pontosDeIbope , styleGrande);
        
    });

    //--INICIO DAS FUNCÇOES DO JOGO--
    //monta cartas do cartas do oponente
    function geraCartasParaOponente()
    {
        var cardsParaOponete = {};
        for(i in cardsOnField)
        { 
            if(cardsOnField[i].card != null)
            {
                var carta = {
                porsArray : i,
                card     : {
                        name        : cardsOnField[i].card.name,
                        cost        : cardsOnField[i].card.cost,
                        type        : cardsOnField[i].card.type,
                        attack      : cardsOnField[i].card.attack,
                        defense     : cardsOnField[i].card.defense,
                        doOponete   : true
                    } 
                };
                cardsParaOponete[i] = carta;
            }else
            {
                var carta = {
                    porsArray : i,
                    card     : null
                };
                cardsParaOponete[i] = carta;
            }
        }
        //console.log(cardsParaOponete);
        $scope._socket.emit('cardsParaOponete', {
            id_room     : $scope.dados.challenger.socket_id +''+  $scope.dados.challenged.socket_id,
            paraOponete : { socket_id : $scope._socket.id,},
            cards       : cardsParaOponete
        });
    } 

    function montaCardsOponente(cards)
    {
        for(i in cardsOnFieldOponent)
        {
            if(cardsOnFieldOponent[i].card != null)
            {
                cardsOnFieldOponent[i].card.destroy();
                cardsOnFieldOponent[i].card = null;
            }

            if(cards[i].card != null)
            {   
                var newCard = createCard(cards[i], cardsOnFieldOponent[i].PosX , cardsOnFieldOponent[i].PosY);
                cardsOnFieldOponent[i].card = newCard.sprite;
            }
        }
    }
    //Mostra os cards na mão
    function cardOnHand()
    {
        for (var i = 0; i < cardsHand.length; i++) {
            cardsHand[i].card;
        }
    }
    //Ao clicar no deck o usuario cria uma carta na mão do jogador
    function clickOnDeck()
    {
        //console.log(cardsHand);
        if(!comprou)
        {
            for (var i = 0; i < cardsHand.length; i++)
            {
                if (cardsHand[i].card == null)
                {
                    //retira o ultimo elemento
                    var cardData = cards.pop();
                    
                    // gera a carta com a imagem e os dados
                    var card = createCard(cardData, cardsHand[i].PosX, cardsHand[i].PosY);

                    card.sprite.posN = i; 
                    cardsHand[i].card = card;
                    cardsAux.push(card);
                    comprou = true;
                    //console.log(card);
                    break;
                }
            }
        }else
        {
            alert("VOCE JA COMPROU NESSE TURNO");
        }
    }
    
    function up() {
        console.log('button up', arguments);
    }

    function over() {
        console.log('button over');
    }

    function out() {
        console.log('button out');
    }

    //função para criar a carta
    function createCard(cardData, PosX, PosY)
    {

        var card = {sprite    : game.add.sprite(PosX, PosY, 'card')}
        card.sprite.width   = 100;
        card.sprite.height  = 150;
        card.sprite.name    = cardData.card.name;
        card.sprite.cost    = cardData.card.cost;
        card.sprite.type    = cardData.card.type;
        card.sprite.attack  = cardData.card.attack;
        card.sprite.defense = cardData.card.defense;
        card.sprite.atacou  = false;
        if(cardData.card.doOponete)
        {
            card.sprite.doOponete = true;
        }


        card.noCampo        = false;

        var style        = { font: "10px Arial", fill: "#000000", wordWrap: true, wordWrapWidth: card.sprite.width, align: "center" };
        var style2       = { font: "10px Arial", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: card.sprite.width, align: "center" };
        var style3       = { font: "13px Arial", fill: "#000000", wordWrap: true, wordWrapWidth: card.sprite.width, align: "center" };
        var styleAtcEDef = { font: "70px Comic Sans MS", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: card.sprite.width, align: "center" };

        card.sprite.addChild(game.add.text(25, 130, 'Ataque'  , style2));
        card.sprite.addChild(game.add.text(95, 130, 'Defesa'  , style2));
        card.sprite.addChild(game.add.text(20, 140, cardData.card.attack , styleAtcEDef));
        card.sprite.addChild(game.add.text(90, 140, cardData.card.defense , styleAtcEDef)); 
        
        var image =  game.add.image(15, 25, cardData.card.name);
            image.width = 120;
            image.height = 85;
        card.sprite.addChild(image); 

        //Nome da carta
        var nomeDaCarta = game.add.text(10, 5, cardData.card.name , style3);
        nomeDaCarta.fontWeight = 'bold';
        //nomeDaCarta.stroke = '#000000';
        //nomeDaCarta.strokeThickness = 5;
        card.sprite.addChild(nomeDaCarta);
        //custo da carta
        var cardCost = game.add.text(130, 5, cardData.card.cost    , style3);
        cardCost.fontWeight = 'bold';
        //cardCost.stroke = '#000000';
        //cardCost.strokeThickness = 5;
        card.sprite.addChild(cardCost);
        //tipo da carta
        var cardType = game.add.text(35, 115, 'HUE HUE CARD GAME'   , style);
        cardType.fontWeight = 'bold';
        //cardType.stroke = '#000000';
        //cardType.strokeThickness = 5;
        card.sprite.addChild(cardType);  

        card.sprite.inputEnabled = true;
        card.sprite.events.onInputDown.add(cardPoppup, this);
        dadosOponente();
        return card;
    }


    function cardPoppup(card)
    {
        //console.log(card);
        poppup = game.add.sprite(card.x, card.y, 'cardPopup');
        //poppup.width  = card.width;
        //poppup.height = card.height;
        if(!card.doOponete)
        {
          
            if(card.noCampo != true)
            {
                var botao = game.add.button(5,10, 'btn_summonar', function() {summonaCard(card)}, this, 2, 1, 0);
                botao.width = 90;
                poppup.addChild(botao);
            }else
            {
                var botao = game.add.button(5,10, 'btn_atacar', function() {cardAtaque(card)}, this, 2, 1, 0);
                botao.width = 90;
                poppup.addChild(botao);
            }
        }
          var botao2 = game.add.button(5,55, 'btn_dadosCarta', function() {cardData(card)}, this, 2, 1, 0);
            botao2.width = 90;
            poppup.addChild(botao2);
        
        
        if(!card.doOponete)
        {
            var botao3 = game.add.button(5,100, 'btn_descartar', function() {cardDescartar(card)}, this, 2, 1, 0);
            botao3.width = 90;
            poppup.addChild(botao3);
        }
        poppup.inputEnabled = true;
        poppup.events.onInputOut.add(overPopOut, this);
        //poppup.events.onInputOver.add(overPopOn, this);
    }

    function overPopOut(sprite) {
        if(game.input.x > sprite.x && game.input.x < sprite.x + 149)
        {
            //console.log('ainda to dentro');
        }else{
            sprite.destroy();
        }

        if(game.input.y > sprite.y && game.input.y < sprite.y + 91)
        {
            //console.log('ainda to dentro');
        }else{
            sprite.destroy();
        }
    }

    

    function fecharDados(sprite)
    {
        sprite.destroy();
    }

    //EVENTOS DA CARTA
    //SUMMONAR
    function started() {
    }

    function completed(sprite) {

        sprite.destroy();
    }

    function mensagemAviso(texto)
    {
        var sprite = game.add.image(game.world.centerX, game.world.centerY, 'msg_small');
            sprite.anchor.set(0.5);
            sprite.alpha = 0.1;
            var tween = game.add.tween(sprite).to( { alpha: 1 }, 2000, "Linear", true, 500);
            tween.onStart.add(started, this);
            tween.onComplete.add(completed, this);
    }

    function summonaCard(card) {
        console.log(cardsOnField);
        if($scope.turnoAtivo != $scope._socket.id)
        {
            mensagemAviso();
        }else
        {
            if(card.cost > pontosDeIbope)
            {
                poppup.destroy();
                alert('carta nao pode ser \n summonada');
            }else
            {
                for (var i in cardsOnField)
                {
                    if (cardsOnField[i].card == null)
                    {
                        card.x = cardsOnField[i].PosX;
                        card.y = cardsOnField[i].PosY;
                        card.noCampo = true;
                        cardsOnField[i].ocupada = true;
                        cardsHand[card.posN].card = null;
                        pontosDeIbope = pontosDeIbope - card.cost;
                        cardsOnField[i].card = card;
                        card.posN = i;
                        poppup.destroy()
                        break;
                    }
                }
            }
        }
        geraCartasParaOponente();
        dadosOponente();   
    }

    //DADOS DA CARTA
    function cardData(card)
    {
        var newCard = game.add.sprite(100, 200, card.generateTexture());

        game.world.bringToTop(newCard);
        newCard.scale.setTo(2,2);
        newCard.x = game.world.centerX -190;
        newCard.y = game.world.centerY - 250;
        var botaoFechar = game.add.button(60, 220, 'botaoFecharDadosCarta', function(){fecharDados(newCard)}, this, 2 , 1 ,0);
        botaoFechar.width  = 40;
        botaoFechar.height = 20;
        newCard.addChild(botaoFechar);
        poppup.destroy();
    }

    //ATACAR
    function cardAtaque(card)
    {
        //console.log(card);

        if(!card.atacou)
        {
            card_atacante = 
                {
                    socket_id : $scope._socket.id,
                    id_room   :  $scope.dados.challenger.socket_id +''+  $scope.dados.challenged.socket_id,
                    card      :
                    {
                        name      : card.name,
                        cost      : card.cost,
                        attack    : card.attack,
                        defense   : card.defense,
                        type      : card.type,
                        posN      : card.posN
                    }
                }
                card.atacou = true;
                $scope._socket.emit('atacando', card_atacante);
            }else
            {
                alert('VOCE JA ATACOU COM ESTA CARTA');
            }

        
        poppup.destroy();
        dadosOponente();
    }

    //DESCARTAR
    function cardDescartar(card)
    {
        //alert('metodo de DESCARTE');
        card.destroy();
        cartasGeladeira.push({ nome : card.name});
        if(card.noCampo)
        {
            cardsOnField[card.posN].card = null;
            cardsOnField[card.posN].ocupada = false;
        }else{
            cardsHand[card.posN].card = null;
        }



        poppup.destroy();
        dadosOponente();
        geraCartasParaOponente();
    }

    //metodo para trocar
    function trocaTurno() 
    {
        //console.log('Troca de turno');
        //$scope.dados = $stateParams.dados
        //$scope.turnoAtivo = null;
        var data =
        {
            dados : $stateParams.dados,
            turnoAtivo : $scope.turnoAtivo
        }
        $scope._socket.emit('troca_turno', data);
    }

    function receberDano(cardAtacante, telaAtaque)
    {
        //console.log(cardAtacante.sprite.name +" te acacou e voce perdeu "+ cardAtacante.sprite.attack + " pontos de vida");
        pontosDeVida = pontosDeVida - cardAtacante.sprite.attack;
        telaAtaque.destroy();
        if(pontosDeVida <= 0)
        {
            fimDeJogo(); 
        }
    }

    function defender(defesa)
    {
        var atacante = this.atacante.sprite;
        //console.log('Dados da atacante');
        //console.log(atacante);
        if(atacante.attack >= defesa.defense)
        {   
            cardsOnField[defesa.posN].card.destroy();
            cardsOnField[defesa.posN].card = null;
            cardsOnField[defesa.posN].ocupada = false;
        }
        if(defesa.attack >= atacante.defense)
        {
            //console.log('SE DEFENDENDO COM UMA CARTA BOA!!!');
            $scope._socket.emit('destroi_carta_campo_oponente', {
                posN              : atacante.posN,
                socket_challenger : $scope.dados.challenger.socket_id,
                socket_challenged : $scope.dados.challenged.socket_id
            });
        }
        this.telaAtaque.destroy();
        dadosOponente();
        geraCartasParaOponente();
    }

    function fimDeJogo()
    {

        /* Quando a vida de um jogador chegar a zero ele perde, entao emite um evento para dizer que esta tudo acabado
            O evendo ser emitido com os dados socket
            Passos
                Jogador que perdeu diz que perdeu
                Servidor recebe os dados
                Servidor diz que o outro jogador ganhou
                Jogo finaliza
                Pontos são distribuidos
        */
        $scope._socket.emit('fimDeJogo',{
                id_room : $scope.dados.challenger.socket_id +''+  $scope.dados.challenged.socket_id,
                socket_id : $scope._socket.id
        });

    }



    //Evento para dados do Jodador - CARTAS
    $scope._socket.on('cartas_do_oponente', function(data){
        montaCardsOponente(data);
    });

    //Evento para dados do Jodador - CARTAS
    $scope._socket.on('novo_turno', function(data){
        //console.log(data);
        $scope.turnoAtivo = data;
        comprou = false;
        if($scope.turnoAtivo == $scope._socket.id)
        {
            for(var i in cardsOnField)
            {
                if(cardsOnField[i].card != null)
                {
                    cardsOnField[i].card.atacou = false;
                }
                
            }
            pontosDeIbopeAntes++;
            pontosDeIbope = pontosDeIbopeAntes;
        }
        geraCartasParaOponente();
    });

    $scope._socket.on('Destruindo_carta', function(data){
        cardsOnField[data].card.destroy();
        cardsOnField[data].card = null;
        cardsOnField[data].ocupada = false;
    })

    $scope._socket.on('recebendo_ataque', function(data){
        //console.log('DADOS DO DATA')
        //console.log(data);

        var telaAtaque = game.add.sprite(75, 75, 'telaAtaque');
        var cardAtacante = createCard(data, 25, 85);
        cardAtacante.sprite.posN = data.card.posN;
        cardAtacante.sprite.width = 200;
        cardAtacante.sprite.height = 300;
        cardAtacante.sprite.events.onInputDown.removeAll();
        ////console.log('CARD ATACANTE');
        // console.log(cardAtacante);
        telaAtaque.addChild(cardAtacante.sprite);
    
        for(i in cardsOnField)
        {
            if(cardsOnField[i].card != null)
            {
                if(i < 3)
                {
                    var cardDefesa = game.add.sprite(290 + (i * 100), 120, cardsOnField[i].card.generateTexture());
                    cardDefesa.attack    = cardsOnField[i].card.attack
                    cardDefesa.cost      = cardsOnField[i].card.cost
                    cardDefesa.defense   = cardsOnField[i].card.defense
                    cardDefesa.doOponete = cardsOnField[i].card.doOponete
                    cardDefesa.name      = cardsOnField[i].card.name
                    cardDefesa.type      = cardsOnField[i].card.type
                    cardDefesa.posN      = i;
                    cardDefesa.inputEnabled = true;
                    cardDefesa.events.onInputDown.add(defender, { atacante : cardAtacante, telaAtaque : telaAtaque});
                    cardDefesa.width = 70;
                    cardDefesa.height = 120;
                    //console.log(cardDefesa);
                    telaAtaque.addChild(cardDefesa);

                }else{
                    var cardDefesa = game.add.sprite(40 + (i * 100), 220, cardsOnField[i].card.generateTexture());
                    cardDefesa.attack    = cardsOnField[i].card.attack
                    cardDefesa.cost      = cardsOnField[i].card.cost
                    cardDefesa.defense   = cardsOnField[i].card.defense
                    cardDefesa.doOponete = cardsOnField[i].card.doOponete
                    cardDefesa.name      = cardsOnField[i].card.name
                    cardDefesa.type      = cardsOnField[i].card.type
                    cardDefesa.posN      = i;
                    cardDefesa.inputEnabled = true;
                    cardDefesa.events.onInputDown.add(defender,  { atacante : cardAtacante, defesa : telaAtaque });
                    cardDefesa.width = 70;
                    cardDefesa.height = 120;
                    //console.log(cardDefesa);
                    telaAtaque.addChild(cardDefesa);
                }
                
            }
            try{
                var btn_telaAtque = game.add.button(327.5, 345, 'btn_recebendo_ataque', function(){receberDano(cardAtacante, telaAtaque)}, this, 1 ,0);;
                telaAtaque.addChild(btn_telaAtque);
            }catch(err){
                console.log(err);
            }
        }
    });

    function zeraVariaveis()
    {
        $scope.dados = null
        $scope.turnoAtivo = null;
        cards = [];
        cartasGeladeira = [];
        cardsAux = [];
        button;
        pontosDeVida = 5;
        pontosDeIbope= 50;
        poppup;
        btn_turno;
        deck;
        contador = 0;
        comprou  = false;

        cardsParaOponete = {};
        oponenteVida = 0;
        oponenteIbope = 0;
        oponenteVidaNum = 0;
        oponenteIbopeNum = 0;
        oponenteCartasDeck = null;
        oponenteGeladeira = 0;
    }

    function redirect(){    window.open("perfil", "_self");}
    function redirectPesquisa(){  window.location.assign("https://docs.google.com/forms/d/e/1FAIpQLSczTQBEEZl13Ym3TK3cRaHPG3x-if8aGt9__N_PEq-2ejA_ng/formResponse");}

    $scope._socket.on('perdeu', function(data){

        var style = { font: "bold 40px Comic Sans MS", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" }; 
        var style2 = { font: "bold 10px Comic Sans MS", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" }; 
        
        var telaFum = game.add.sprite(0, 0, "msg_final");
        
        var textoUm = game.add.text(game.world.centerX, 150 , 'QUE PENA VOCE PERDEU!' , style);
        textoUm.anchor.setTo(0.5);
        
        var textPontos = game.add.text(game.world.centerX, 200 , 'PONTOS GANHOS : ' + data.pontos , style);
        textPontos.anchor.setTo(0.5);
        

        var perdeu_why     = game.add.sprite(game.world.centerX, 350, "perdeu_why");
        perdeu_why.width       = 200;
        perdeu_why.height      = 200;
        perdeu_why.anchor.setTo(0.5);

        var botaoPesquisa   = game.add.button(game.world.centerX,480, 'btn_pesquisa_jogo', redirectPesquisa , this, 1, 0, 2);
        botaoPesquisa.anchor.setTo(0.5);
        var botao   = game.add.button(game.world.centerX,535, 'btn_volta_para_perfil', redirect , this, 1, 0, 2);
        botao.anchor.setTo(0.5);
        text = game.add.text(10, 3, "phaser 2.4 text bounds", style2);
        
        $scope._socket.emit('disconnect', { id_room : $scope.dados.challenger.socket_id +''+  $scope.dados.challenged.socket_id })
        zeraVariaveis();

    });
    $scope._socket.on('ganhou', function(data){

        var style = { font: "bold 40px Comic Sans MS", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" }; 
        var telaFum = game.add.sprite(0, 0, "msg_final");
        var textoUm = game.add.text(game.world.centerX, 150 , 'PARABENS VOCE GANHOU' , style);
        textoUm.anchor.setTo(0.5);
        var textPontos = game.add.text(game.world.centerX, 200 , 'PONTOS GANHOS : ' + data.pontos , style);
        textPontos.anchor.setTo(0.5);
        var ganhei_uhuu     = game.add.sprite(game.world.centerX, 330, "ganhei_uhuu");
        ganhei_uhuu.width       = 300;
        ganhei_uhuu.height      = 300;
        ganhei_uhuu.anchor.setTo(0.5);

        var botaoPesquisa   = game.add.button(game.world.centerX,480, 'btn_pesquisa_jogo', redirectPesquisa , this, 1, 0, 2);
        botaoPesquisa.anchor.setTo(0.5);
        var botao   = game.add.button(game.world.centerX,535, 'btn_volta_para_perfil', redirect , this, 1, 0, 2);   
        botao.anchor.setTo(0.5);
        console.log(data);
        $scope._socket.emit('disconnect', { id_room : $scope.dados.challenger.socket_id +''+  $scope.dados.challenged.socket_id })
        zeraVariaveis();
    });

});

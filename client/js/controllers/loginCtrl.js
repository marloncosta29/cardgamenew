angular.module('app').controller('LoginCtrl', function($scope, $location, $rootScope, $state){
    
    $scope.email    = '';
    $scope.password = '';

    $scope.login = function()
    {
        $scope._socket.emit('login', { email : $scope.email, password : $scope.password }, function(data){
            if(data)
            {
                //console.log(data);
                $rootScope.player = data;
                if(data.nickname == 'admin')
                    $state.go("admin");
                else{
                    $state.go("perfil");
                }
            }else{
                    $state.go("home");
            }         
        });
    }    
});
angular.module('app').controller('appPubNub', [ '$scope', 'Pubnub', function ($scope, Pubnub) {

    $scope.uuid = Math.random(100).toString();
    $scope.channel = 'perfil'
    

    Pubnub.init({
        publish_key: 'pub-c-0821f8c7-f987-4c99-94a8-948bb5698fb5',
        subscribe_key: 'sub-c-72002f08-8b2e-11e6-a68c-0619f8945a4f',
        uuid: $scope.uuid
    });

    Pubnub.subscribe({
        channel: $scope.channel,
        triggerEvents: ['callback']
    });

    Pubnub.publish({
        channel: $scope.channel,
        message:
        {
            nome: 'user',
            id: Math.random(100) * 100, 
            age: 99,
            nivel: 'testeNivel'
        },
        callback: function (m) {
            console.log(m);
        } 
    })

    $scope.$on(Pubnub.getMessageEventNameFor($scope.channel), function (ngEvent, m) {
        $scope.$apply(function () {
            console.log(m);
            $scope.users.push(m);
        });
    });
}]);

angular.module('app').controller('perfilCtrl', function($http, $scope, $location, $rootScope, $state, $stateParams){
    console.log('Carregando o CONPROLLER PERFIL')
    console.log($stateParams);
    if($rootScope.player == null)
    {
        $http.get('userData').success(function(data){
            $rootScope.player = {
                hash      : data.hash,
                user      : data.userData,
                socket_id : $scope._socket.id,
                deck      : null
            }
            $scope.dataPlayer = $rootScope.player;
            $scope.users = [];
            $scope.modalDesafio = false;
            $scope._socket.emit('dataToList', $rootScope.player);
        });
    }
    if($stateParams.data)
    {
        alert('CAIO DOS DADOS PARA ATUALIZAR');
        $scope._socket.emit('atalizaDados', $rootScope.player.user._id, function(err, data){
            if(err)
            {
                alert('Houve um erro ao atualizar os dados apos o jogo, atualize a pagina');
            }else{
                $rootScope.user = data;    
            }
        });
    }
    //data atual
    $scope.dataAtual = function() 
    {
        var meses = new Array("Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro");
        var semana = new Array("Domingo","Segunda-feira","Terça-feira","Quarta-feira","Quinta-feira","Sexta-feira","Sábado");
    
        var hoje = new Date();
        var dia = hoje.getDate();
        var dias = hoje.getDay();
        var mes = hoje.getMonth();
        var ano = hoje.getYear();
        if (navigator.appName == "Netscape")
            var ano = ano + 1900;
        var diaext = semana[dias] + ", " + dia + " de " + meses[mes] + " de " + ano;
        return diaext;
    }
    //pegando os dados dos jogadores
    $scope._socket.on('players', data => {
        $scope.$apply(() => $scope.users = data);
    });
    //Desafiando o adversario
    $scope.toChallenger = function(user, player){
        $scope._socket.emit('toChallenger', { id: player.socket_id + "" + user.socket_id, challenger: player, challenged: user, challengeAccepted: false });
    };
    //recebedo o desafio
    var cont = 0

    $scope._socket.on('challenging', function (data) {
            $('#myModal').modal('show');
            $('#textDesafio').text('VOCE ESTA SENDO DESAFIADO POR\n' + data.challenger.user.nickname);
            $('#btn_aceito').click(function(){
                console.log(data);
                $scope._socket.emit('accept', data); 
                $('#myModal').modal('hide'); 
            });
    });
    //começa o jogo
    $scope._socket.on('gameStart', function (data) {
            console.log('INICIANDO O JOGO');
            console.log(data);
            $state.go('game', {dados : data}, {reload: true});
    });
    //Pega as cartas do banco referente ao jogador
    $scope.cartasJogador = function(id_jogador)
    {
        console.log('ID PARA AS CARTAS');
        console.log(id_jogador);
        $scope._socket.emit('cartasDoJogador', { id : id_jogador }, function(data){
            console.log('ENTREI NO SOCKET DE CARTAS');
            $scope.dados = data;
        });
    }

    $scope.resolution = screen.width;
    if($scope.resolution <= 800)
    {
        $("#perfilLeft").hide();
    }

    $('#btn_postar').click(function(){
        
        var date = new Date();
        console.log(date);

        var post = {
            id       : $rootScope.player.user._id,
            icone    : '../images/site/iconUserPost.png',
            titulo   : $rootScope.player.user.nickname + ' diz...',
            conteudo : $scope.postContent, 
            dia      : date.getDate(),
            mes      : date.getMonth(),
            ano      : date.getFullYear(),
        }
        console.log(post);

        $scope._socket.emit('post', post, function(data){
            console.log(data);
        });
        $scope.postContent = '';
        $('#feedNoticia').modal('hide');
    });

    $scope._socket.on('getFeeds', function(data){
        console.log(data);
        $scope.feeds = data.reverse().slice(0, 10);
     })

    $scope._socket.emit('carregaFeed', {});

    $scope.getMouthExtense = []
    $scope.getMouthExtense.push('Janeiro')
    $scope.getMouthExtense.push('Fevereiro')
    $scope.getMouthExtense.push('Março')
    $scope.getMouthExtense.push('Abril')   
    $scope.getMouthExtense.push('Maio')
    $scope.getMouthExtense.push('Junho')
    $scope.getMouthExtense.push('Julho')
    $scope.getMouthExtense.push('Agosto')
    $scope.getMouthExtense.push('Setembro')
    $scope.getMouthExtense.push('Outubro')
    $scope.getMouthExtense.push('Novembro')
    $scope.getMouthExtense.push('Desembro')

    //console.log($scope.getMouthExtense)
	
	$scope.trocaImgPerfil = function(idUser, caminho)
	{
            dados = {
                id : idUser,
                url: caminho
            }
            console.log(dados);
		 $rootScope.player.user.perfilImage = caminho;
		 $scope._socket.emit('trocaImgPerfil', dados);

	}
	
	$scope.ranking = [];
	$scope.getRanking = function()
	{
		console.log('ENRTREI NO RANKING');
		$scope._socket.emit('getRanking', {} , function(data){
			console.log(data);
			$scope.ranking = data;
		});
	}
	
	$scope.dadosUsuarioOn = function(dados){
		console.log(dados);
		$('#dadosUserOnline').modal('show');
		$('#nickNameModalOnline').text(dados.user.nickname);
		$("#imageModalOnline").attr("src", dados.user.perfilImage);
        $('#nomeModalOnline').text("Nome : " + dados.user.name);
        $('#nivelModalOnline').text("Nivel : " + dados.user.nivel);
        $('#partidasGanhasModalOnline').text("Partidas Ganhas" + dados.user.win);		
        $('#partidasPerdidasModalOnline').text("Partidas Perdidas : " + dados.user.lose);
        $('#xpTotalModalOnline').text("Total de XP ganho : " + (dados.user.nivel * 1000 + dados.user.experience));
        $('#historiaModalOnline').text(dados.user.name);
	}

    $scope.atualizaDados = function (dados)
    {
        
    }
     
});
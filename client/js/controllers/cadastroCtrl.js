angular.module('app').controller('SavePlayerCtrl', function($scope){
    console.log($scope._socket);
    //$scope.name         = '';
    //$scope.email        = '';
    //$scope.password     = '';
    //$scope.passwordConf = '';
    
    //$scope.save = function()
    //{
    //    $scope._socket.emit('savePlayer', { name : $scope.name, email : $scope.email, password : $scope.password });
    //}

    validator.message.date = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
    .on('blur', 'input[required], input.optional, select.required', validator.checkField)
    .on('change', 'select.required', validator.checkField)
    .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required').on('keyup blur', 'input', function() {
    validator.checkField.apply($(this).siblings().last()[0]);
    });

    $('form').submit(function(e) {
    e.preventDefault();
    var submit = true;

    // evaluate the form using generic validaing
    if (!validator.checkAll($(this))) {
        submit = false;
    }

    if (submit)
        $scope._socket.emit('savePlayer', 
        { name     : $scope.name,
          nickname : $scope.nickname, 
          email    : $scope.email, 
          password : $scope.password 
        });
        //this.submit();
    return false;
    });


});
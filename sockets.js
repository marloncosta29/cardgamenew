var load = require('express-load');

module.exports = function(io)
{
	load('server/socket').into(io);
	return io;
}
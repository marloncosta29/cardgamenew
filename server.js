﻿//Constantes da sessão
const KEY = 'huehuecockie';
const SECRET = 'huehuesecret123';

//variaveis da aplicação
var express          = require("express");
var app              = express();
var serv             = require("http").Server(app);
var util             = require('util');
var favicon          = require('serve-favicon');
var cookieParser     = require('cookie-parser');
var cookie           = cookieParser(SECRET);
var bodyParser       = require('body-parser');
var load             = require('express-load');
var expressSession   = require('express-session');
var flash            = require('express-flash');
var moment           = require('moment');
var expressValidator = require('express-validator');
var cons             = require('consolidate');
var router           = require('./server/router/routers')
var routerAdmin      = require('./server/router/routerAdmin')
//logs
var fs = require('fs');
var morgan = require('morgan');
var path = require('path');

// create a write stream (in append mode)
//var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'})

// setup the logger
//app.use(morgan('combined', {stream: accessLogStream}));


//variaveis de jogo
//var Player    = require("./server/models/Player");
//var Game      = require("./server/models/Game");
//var PlayerDao = require('./server/dao/PlayerDao');

//Conexão com o banco de dados
var mongoose  = require('mongoose');
//mongoose.connect('mongodb://127.0.0.1/huehuegame');
mongoose.connect('mongodb://cardgame:cardgame123@ds055525.mlab.com:55525/cardgame');

//engine
app.engine('html', cons.swig);
app.set('view engine', 'html');
app.set('views', __dirname + '/client/views');

//Uses da aplicação
app.use(express.static(__dirname + "/client"));
app.use(favicon(__dirname + '/huehueico.png'));
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(expressValidator());
app.use(cookie);

// Configuring Passport
var passport = require('passport');
app.use(expressSession({
    secret: 'huehuehueGame',
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());  


//Rota fixa para o main
app.use('/admin', routerAdmin);
app.use('/', router);

//execução do servidor Node e porta de conexão
var port = process.env.port || 1337;
var io        = require("socket.io")(serv);
io.listen(serv.listen(port, function (req, res) {
    console.log('Server running on port: ' + port);
}));


require ('./sockets')(io);
